/* Copied from https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript */
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
/* Copied from https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript */

function fetchAstroApi(sign) {
    const baseUrl = 'https://sameer-kumar-aztro-v1.p.rapidapi.com/';
    const apiOptions = {
        method: 'POST',
        headers: {
            'X-RapidAPI-Key': '3dcf30d2eemshcf66d916efdfa72p1a1e8fjsn0f1b7e9a511f',
            'X-RapidAPI-Host': 'sameer-kumar-aztro-v1.p.rapidapi.com'
        }
    };

    return fetch(`${baseUrl}?sign=${sign}&day=today`, apiOptions);
}

async function setResponse(response) {
    data = await response.json();

    els['result'].style.visibility = "visible";
    let sI = els['select-sign'].selectedIndex;
    els['span-sign'].textContent = els['select-sign'].options[sI].text;
    els['span-date'].textContent = data.current_date;
    els['span-luckyNumber'].textContent = data.lucky_number;
    els['span-description'].textContent = data.description;
}

function validateSign(sign) {
    let optionArray = Array.from(els['select-sign'].options)
    return optionArray.find(option => option.value == sign);
}

const el_ids = [
    'select-sign',
    'btn-copy',
    'result',
    'span-sign',
    'span-date',
    'span-luckyNumber',
    'span-description'
];

const els = {};
el_ids.forEach((id) => {
    els[id] = document.getElementById(id);
});

let presetSign = getCookie('sign');
const urlParams = new URLSearchParams(window.location.search);
if (
    urlParams.has('sign')
    && validateSign(urlParams.get('sign'))
) {
    presetSign = urlParams.get('sign');
}
//Remove sign Parameter after processing it
window.history.replaceState({}, document.title, window.location.pathname);

if (presetSign) {
    els['select-sign'].value = presetSign;
}

// Initial Api Call
fetchAstroApi(els['select-sign'].value)
    .then(response => setResponse(response))
    .catch(err => console.log(err));

els['select-sign'].addEventListener('change', () => {
    var sign = els['select-sign'].value;
    setCookie('sign', sign, 30);
    fetchAstroApi(sign)
        .then(response => setResponse(response))
        .catch(err => console.log(err));
});

els['btn-copy'].addEventListener('click', () => {
    const origin = window.location.href;
    const param = `?sign=${els['select-sign'].value}`;
    const url = `${origin}${param}`;
    navigator.clipboard.writeText(url);
    window.alert(`Copied!\n${url}`);
});
